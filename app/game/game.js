(function($) {

	// check if page has jquery ui
	if (typeof jQuery.ui === 'undefined') {
		// add jquery ui js
		var jqui = document.createElement("script");
		jqui.type = "text/javascript";
		jqui.src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js";
		$('body').append(jqui);

		// add jquery ui css
		$("head").append('<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">');
	}

	// check if the page has bootstrap 3
	if (typeof $().emulateTransitionEnd !== 'function') {
		// add bootstrap js
		var bootstrap = document.createElement("script");
		bootstrap.type = "text/javascript";
		bootstrap.src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js";
		$("body").append(bootstrap);

		// add bootstrap css
		$("head").append('<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">');
	}

	// add custom css for game
	$("head").append('<link rel="stylesheet" href="game/game.css">');

	// fn is essentially a shortcut to prototype
    $.fn.mazify = function() {
        return this.each(function() {
			
			// empty container and fill it with the game html
			$(this).empty();
            $(this).load('game/game.html', function() {
            	var start, end;
			    var run = null;
			    var table_rows, table_columns;

			    ////////////////// FIRST DEFINE THE FIVE FUNCTIONS //////////////////

				// change cells into black or white by clicking on them
				function toggleBlackAndWhite() {
					// get specific cell
					var id = $(this).attr('id');
					var cell = '#'+id;

					// toggle between black and white
					if ($(cell).attr('bgcolor') === 'white') {
						$('#'+id).attr('bgcolor', 'black');
					} else {
						$('#'+id).attr('bgcolor', 'white');
					}
				}

				// build the grid
				function makeTable(rows, columns) {
					var newTable = '<table table-layout="fixed" align="center" border="1">';

					// each loop makes one row
					for (var i=1 ; i<=rows ; i++) {
						var tds = '';
						for (var j=1 ; j<=columns ; j++) {
							tds += "<td class='td' id='r" + i + "c" + j + "' bgcolor='white'></td>";
						}
						newTable += "<tr>" + tds + "</tr>";
					}

					newTable += "</table>";

					return newTable;
				}

				// make the game
				function createGame(containerId) {
					start = null;
					end = null;
					clearInterval(run);

					var row_num = $('#rows').val(),
							column_num = $('#columns').val(),
							container = '#' + containerId;
			            
		            // store values globally for future reference (in bot navigation)
		            table_rows = row_num;
		            table_columns = column_num;
		            
		            // initialize grid
					$('#mazeInit').modal('hide');
					$(container).empty();
					$(makeTable(row_num, column_num)).hide().appendTo(container).fadeIn(1000);
					
					// fix cell ratio when squeezed
					if ($('.td').width() < 50) {
						$('.td').css('height', $('.td').width()+1);
					}

					// show instructions
					var info = "<div class='row' id='info'><label class='label label-default'>Click on the squares to turn them into obstacles</label><button type='button' id='ready' class='btn btn-success'>Ready!</button></div>";
					$(info).hide().appendTo(container).fadeIn(1000);

					// add event listener for changing cell color
					$('.td').on('click', toggleBlackAndWhite);

					$('#ready').on('click', function() {

						// let the grid (only white cells) accept the icons
						$(".td[bgcolor='white']").droppable({
							accept: '#bot , #bot_clone , #target, #target_clone',
							drop: function(event, ui) {
								var id = $(ui.draggable).attr('id');

								// place image and update start and end references
								if (id === 'bot' || id === 'bot_clone') {
									$(this).append("<div id='bot_clone' class='bot_clone'><img src='icons/bot.png' width=100% height=100% /></div>");
									$('.bot_clone').draggable({revert: 'invalid'});
									start = $(this).attr('id');
								}
								else if (id === 'target' || id === 'target_clone') {
									$(this).append("<div id='target_clone' class='target_clone'><img src='icons/target.png' width=100% height=100% /></div>");
									$('.target_clone').draggable({revert: 'invalid'});
									end = $(this).attr('id');
								}

								// making sure that multiple copies of the icons don't appear
								if (id === 'bot' || id === 'target') {
									$('#'+id).draggable('disable');
								}
								else if (id === 'bot_clone' || id === 'target_clone') {
									$(ui.draggable).remove();
								}								
							}
						});

						// change instructions
						$('#info').empty();
						var moreInfo = "<label id='dragIconsLabel' class='label label-default'>Place the bot and the target on the grid</label><button type='button' class='btn btn-success' id='done'>Done</button>";
						$(moreInfo).hide().appendTo('#info').fadeIn(600);

						// obstacles not editable any more
						$('.td').off('click');
						
						// setup is done, ready to play
						$('#done').click(function() {

							// don't let the user start if icons not on grid
							if (start === null || end === null) {
								$('#dragIconsLabel')
									.removeClass('label-default')
									.addClass('label-danger');
							} else {
								// cannot edit icon positions any more
								$('#bot_clone, #target_clone').each(function() {
									$(this).draggable('disable');
								});

								// update instructions
								$('#info').empty();
							    $('<h5>Click on Solve to play the game</h5>').hide().appendTo('#info').fadeIn(600);

								// show solve button and add event listener
								$('#solve').fadeIn(600).removeClass('hidden');
							    $('#solve').click(function() {
							    	// start solving
							    	clearInterval(run);
							    	run = setInterval(nextStep, 500);

							    	// hide instructions
							    	$('#info').remove();

							    	// hide new and solve buttons
							    	$('#new').addClass('hidden');
							    	$('#solve').addClass('hidden');

							    	// show stop button and add listener
							    	$('#stop').fadeIn(600).removeClass('hidden');
							    	$('#stop').click(function() {
							    		// stop bot
							    		clearInterval(run);

							    		// show new and stop buttons
							    		$('#new').fadeIn(600).removeClass('hidden');
							    		$('#stop').addClass('hidden');

							    		// show continue button and add listener
							    		$('#continue').fadeIn(600).removeClass('hidden');
							    		$('#continue').click(function() {
							    			// continue solving
							    			clearInterval(run);
							    			run = setInterval(nextStep, 500);

							    			// hide continue and show new and stop buttons again
							    			$('#continue').addClass('hidden');
							    			$('#new').addClass('hidden');
							    			$('#stop').fadeIn(600).removeClass('hidden');
							    		});
							    	});
							    });
							}
						});
					});
				}

				// decide where the bot should go next
				function nextStep() {
					// store position of bot
				    var current       = start ,
				    	currentRow    = current.substring(1,current.indexOf('c')) ,
				        currentColumn = current.substring(current.indexOf('c')+1) ;
				    
				    // get possible places to go from here  (and check that these places exist, eg. column 11 in a table with 10 columns)
				    var up    = (Number(currentRow) - 1) > 0                 ? 'r' + String(Number(currentRow) - 1) + 'c' + currentColumn : undefined ,
				        down  = (Number(currentRow) + 1) <= table_rows       ? 'r' + String(Number(currentRow) + 1) + 'c' + currentColumn : undefined ,
				        right = (Number(currentColumn) + 1) <= table_columns ? 'r' + currentRow + 'c' + String(Number(currentColumn) + 1) : undefined ,
				        left  = (Number(currentColumn) - 1) > 0              ? 'r' + currentRow + 'c' + String(Number(currentColumn) - 1) : undefined ;

				    var positions = [right, down, up, left],
				        been = [],		// has been to green
				        notBeen = [];	// has not been to white
				    
				    // for each place there is to go    
			        for (var k=0 ; k<positions.length ; k++) {
			            var pos = positions[k] ;
			            
			            // if the bot found the target, end game
			            if (pos === end) {
			            	// stop bot
			                clearInterval(run);

			                // hide all buttons, apart from new
			                $('#solve').addClass('hidden');
			                $('#stop').addClass('hidden');
			                $('#continue').addClass('hidden');
			                $('#new').fadeIn(600).removeClass('hidden');

			                // throw appropriate message
			                $('#endGame').modal('show');

			                // end loop
			                return true;
			            } else {
			                var color = $('#'+pos).attr('bgcolor');
			                
			                if 		(color === 'black') { /* do nothing */	 }
			                else if (color === 'green') { been.push(pos);	 }
			                else if (color === 'white') { notBeen.push(pos); }
			            }
			        }
			        
			        var index, go;

			        // give preference to white squares
			        if (notBeen.length > 0) {
			            index = Math.floor(Math.random() * notBeen.length);
			            go = notBeen[index];
			            newPosition(current, go);
			        }
			        else if (been.length === 0) {
			        	clearInterval(run);

			            $('#solve').addClass('hidden');
			            $('#stop').addClass('hidden');
			            $('#continue').addClass('hidden');
			            $('#new').fadeIn(600).removeClass('hidden');

			            // throw appropriate modal
			            $('#deadEnd').modal('show');
			        }
			        else {
			            index = Math.floor(Math.random() * been.length);
			            go = been[index];
			            newPosition(current, go);
			        }
				}

				// update position
				function newPosition(current, pos) {
				    // delete image from current and make it green
				    $('#'+current).empty();
				    $('#'+current).attr('bgcolor', 'green');

				    // add image to new position
				    $('#'+pos).append("<div id='bot_clone' class='bot_clone'><img src='icons/bot.png' width=100% height=100% /></div>");
				    
				    // update bot position reference
				    start = pos;
				}

				////////////////// ADD SOME INITIAL EVENT LISTENERS //////////////////

				$('#create').click(function() {
					var inputOk;
					$('#feedback').empty();		// delete previous error messages

					// rows and columns must be a number larger than or equal to 2
					if ( $('#rows').val() < 2 || isNaN(Number($('#rows').val())) || $('#columns').val() < 2 || isNaN(Number($('#columns').val())) ) {
						$('#feedback').empty();
						$('#feedback').append('<p class="error">Input must be a number larger than or equal to 2</p>');
					} else {
						inputOk = true;
					}

					if (inputOk) {
						$('#solve').addClass('hidden');
						$('#stop').addClass('hidden');
						$('#continue').addClass('hidden');
						createGame('game');
					}
				});

				// let the icons be draggable
				$('#bot').draggable({helper: 'clone', revert: 'invalid'});
				$('#target').draggable({helper: 'clone', revert: 'invalid'});

				// if the draggable icons had been disabled, re-enable them
				$('#new').click(function() {
					$('#bot').draggable('enable');
					$('#target').draggable('enable');
				});
            });
        });
    };
}(jQuery));